import flask


webapp = flask.Blueprint('webapp', __name__)


@webapp.route('/', methods=['GET'])
def index():

    """ Render the HTML template corresponding the webapp """

    return flask.render_template('index.html')
